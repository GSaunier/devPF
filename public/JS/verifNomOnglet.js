$("#addB_ajouter").click(function () {
        $.ajax({
            url: "/ajax/verifNomOnglet/"+$("#addB_form").val(),
            success: function (data) {
                if(data === "false") {
                    M.toast({html: "Le nom de l'onglet existe deja", classes: "toastErreur"});
                } else
                    $('form[name="addB"]').submit();
            },
            error: function (data) {
                var erreur = data.responseJSON.erreur;
                M.toast({html: erreur, classes: "toastErreur"});
            }
        });
        return false;
    }
);

var sub = false;
$('form[name="modOnglet"]').submit(function () {
        if (sub)
            return true;
        var form = $(this);
        $.ajax({
            url: "/ajax/verifNomOnglet/"+ form.find("#modOnglet_nom").val() + "/" + form.children("#modOnglet_idPage").val(),
            success: function (data) {
                if(data === "false") {
                    M.toast({html: "Le nom de l'onglet existe deja", classes: "toastErreur"});
                } else {
                    sub = true;
                    form.submit();
                }
            },
            error: function (data) {
                var erreur = data.responseJSON.erreur;
                M.toast({html: erreur, classes: "toastErreur"});
            }
        });
        return false;
    }
);