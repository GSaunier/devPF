$( document ).ready(function () {
    $(".dropdown-button").dropdown({constrainWidth: false });
    $('.slider').slider({
        height: 450
    });
    $('.carousel').carousel();
    $('.tabs').tabs();
    $('.modal').modal({
        container: 'body'
    });
    $('.datepicker').datepicker({
        yearRange: 100,
        i18n: {
            cancel: 'Retour',
            months: [ 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre' ],
            monthsShort: [ 'Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec' ],
            weekdays: [ 'Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi' ],
            weekdaysShort: [ 'Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam' ],
            weekdaysAbbrev: [ 'D', 'L', 'M', 'M', 'J', 'V', 'S' ],
            clear: 'Effacer'

        },
        firstDay: 1,
        showClearBtn: true,
        format: 'dd-mm-yyyy',
        container: 'body'
    });
    $('input.text-input, textarea#textarea2').characterCounter();
    $('select').formSelect({
        container: 'body'
    });
    $('select').formSelect();
    $('.materialboxed').materialbox();
    $('#demo-component').colorpicker({
        component: '.btn'
    });
});

