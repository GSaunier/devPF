-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 24 fév. 2019 à 12:28
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `routethermale_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `cartes`
--

DROP TABLE IF EXISTS `cartes`;
CREATE TABLE IF NOT EXISTS `cartes` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `circuit`
--

DROP TABLE IF EXISTS `circuit`;
CREATE TABLE IF NOT EXISTS `circuit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `distance` int(11) NOT NULL,
  `nb_max_coureur` int(11) DEFAULT NULL,
  `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1325F3A6591CC992` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `circuit`
--

INSERT INTO `circuit` (`id`, `course_id`, `distance`, `nb_max_coureur`, `nom`, `prix`) VALUES
(3, 1, 135, NULL, 'Grand boucle avec repas', 30),
(4, 1, 135, NULL, 'Grande boucle sans repas', 22),
(5, 1, 88, NULL, 'Petite boucle avec repas', 30),
(6, 1, 88, NULL, 'Petite boucle sans repas', 22);

-- --------------------------------------------------------

--
-- Structure de la table `contenus`
--

DROP TABLE IF EXISTS `contenus`;
CREATE TABLE IF NOT EXISTS `contenus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_ref_id` int(11) DEFAULT NULL,
  `ordre` smallint(6) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contenu_ref_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ADE120364E61B7B7` (`page_ref_id`),
  KEY `IDX_ADE12036852B319B` (`contenu_ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contenus`
--

INSERT INTO `contenus` (`id`, `page_ref_id`, `ordre`, `type`, `contenu_ref_id`) VALUES
(1, 1, 3, 'textes', NULL),
(2, 1, 2, 'sliders', NULL),
(5, NULL, 1, 'images', 2),
(6, NULL, 2, 'images', 2),
(7, 1, 1, 'textes', NULL),
(8, 2, 1, 'textes', NULL),
(10, 2, 2, 'liens', NULL),
(13, 3, 1, 'engagement', NULL),
(14, NULL, 3, 'images', 2);

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_ouverture` datetime DEFAULT NULL,
  `date_fermeture` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course`
--

INSERT INTO `course` (`id`, `nom`, `date`, `description`, `date_ouverture`, `date_fermeture`) VALUES
(1, 'Route thermale', '2019-05-01', 'Route thermale du 1 Mai 2019', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `engagement`
--

DROP TABLE IF EXISTS `engagement`;
CREATE TABLE IF NOT EXISTS `engagement` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_D86F0141591CC992` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `engagement`
--

INSERT INTO `engagement` (`id`, `course_id`) VALUES
(13, 1);

-- --------------------------------------------------------

--
-- Structure de la table `galeries`
--

DROP TABLE IF EXISTS `galeries`;
CREATE TABLE IF NOT EXISTS `galeries` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL,
  `chemin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `images`
--

INSERT INTO `images` (`id`, `chemin`, `nom`) VALUES
(5, '/Images/Slider/cyclistes.jpg', 'image'),
(6, '/Images/Slider/titre2.png', 'image2'),
(14, '/Images/Slider/logo.png', 'logo');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

DROP TABLE IF EXISTS `inscription`;
CREATE TABLE IF NOT EXISTS `inscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circuit_id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_naissance` date NOT NULL,
  `categorie` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `licence_certif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(70) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  `telephone` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payer` tinyint(1) NOT NULL,
  `mail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E90F6D6CF2182C8` (`circuit_id`),
  KEY `IDX_5E90F6D6591CC992` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`id`, `circuit_id`, `nom`, `prenom`, `date_naissance`, `categorie`, `club`, `licence_certif`, `adresse`, `code_postal`, `ville`, `course_id`, `telephone`, `payer`, `mail`) VALUES
(10, 3, 'Saunier', 'Guillaume', '1994-01-04', 'Homme', 'dfsdf', 'fsd', 'sfdsd', 'fds', 'sfd', 1, 'sdfs', 0, 'dfsf@dsf.fd'),
(11, 3, 'Saunier', 'Guillaume', '1994-01-04', 'Homme', 'dfsdf', 'fsd', 'sfdsd', 'fds', 'sfd', 1, 'sdfs', 0, 'dfsf@dsf.fd');

-- --------------------------------------------------------

--
-- Structure de la table `liens`
--

DROP TABLE IF EXISTS `liens`;
CREATE TABLE IF NOT EXISTS `liens` (
  `id` int(11) NOT NULL,
  `chemin` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentaire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pj` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `liens`
--

INSERT INTO `liens` (`id`, `chemin`, `nom`, `commentaire`, `pj`) VALUES
(10, '/Images/pieceJointes/Réglement-Course-2018.pdf', 'Règlement de la course', 'Bonjour à tous, vous trouverez le lien précisant le règlement de notre épreuve', 1);

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190129003834', NULL),
('20190129184407', NULL),
('20190215105952', NULL),
('20190220145443', '2019-02-20 14:54:58'),
('20190220153953', '2019-02-20 15:39:57'),
('20190224122505', '2019-02-24 12:25:13'),
('20190224122612', '2019-02-24 12:26:15');

-- --------------------------------------------------------

--
-- Structure de la table `onglets`
--

DROP TABLE IF EXISTS `onglets`;
CREATE TABLE IF NOT EXISTS `onglets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_ref_id` int(11) DEFAULT NULL,
  `onglet_ref_id` int(11) DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordre` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_44CDFB014E61B7B7` (`page_ref_id`),
  KEY `IDX_44CDFB012EA6B36F` (`onglet_ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `onglets`
--

INSERT INTO `onglets` (`id`, `page_ref_id`, `onglet_ref_id`, `nom`, `ordre`) VALUES
(1, 1, NULL, 'Accueil', 1),
(21, 3, NULL, 'Engagement', 4);

-- --------------------------------------------------------

--
-- Structure de la table `option_course`
--

DROP TABLE IF EXISTS `option_course`;
CREATE TABLE IF NOT EXISTS `option_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix` double NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_55CE6D6E591CC992` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `option_course`
--

INSERT INTO `option_course` (`id`, `titre`, `prix`, `type`, `course_id`) VALUES
(16, 'Repas adulte', 10, 'quantite', 1),
(17, 'Repas enfant', 8, 'quantite', 1);

-- --------------------------------------------------------

--
-- Structure de la table `option_inscri`
--

DROP TABLE IF EXISTS `option_inscri`;
CREATE TABLE IF NOT EXISTS `option_inscri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inscription_id` int(11) NOT NULL,
  `option_course_id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E1178C495DAC5993` (`inscription_id`),
  KEY `IDX_E1178C498887D912` (`option_course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `option_inscri`
--

INSERT INTO `option_inscri` (`id`, `inscription_id`, `option_course_id`, `quantite`) VALUES
(16, 10, 16, 5),
(17, 10, 17, 5),
(18, 11, 16, 5),
(19, 11, 17, 5);

-- --------------------------------------------------------

--
-- Structure de la table `option_quantite`
--

DROP TABLE IF EXISTS `option_quantite`;
CREATE TABLE IF NOT EXISTS `option_quantite` (
  `id` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `option_quantite`
--

INSERT INTO `option_quantite` (`id`, `quantite`, `max`) VALUES
(16, 50, 10),
(17, 50, 10);

-- --------------------------------------------------------

--
-- Structure de la table `option_simple`
--

DROP TABLE IF EXISTS `option_simple`;
CREATE TABLE IF NOT EXISTS `option_simple` (
  `id` int(11) NOT NULL,
  `choix` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pages`
--

INSERT INTO `pages` (`id`, `nom`, `type`) VALUES
(1, 'Accueil', 1),
(2, 'Epreuves route thermale', 1),
(3, 'Engagement', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sliders`
--

INSERT INTO `sliders` (`id`) VALUES
(2);

-- --------------------------------------------------------

--
-- Structure de la table `textes`
--

DROP TABLE IF EXISTS `textes`;
CREATE TABLE IF NOT EXISTS `textes` (
  `id` int(11) NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `textes`
--

INSERT INTO `textes` (`id`, `titre`, `commentaire`) VALUES
(1, 'La route thermale cycliste & cyclo-cross', 'L’Association La Route Thermale cycliste (ARTC) a été créée en 2014 avec pour objectif de développer et organiser des événements de cyclisme sur route, VTT et cyclocross sur le territoire de la plaine des Vosges. Les organisations proposées par ARTC s’adresse à un public varié, licencié ou non.<br><br>\r\n\r\ndepuis 2015, l\'association a ainsi organisé avec succès, son épreuve phare, LA ROUTE THERMALE CYCLISTE, un CYCLO CROSS et le championnat de France des Elus VTT et course à pied !'),
(7, 'Bienvenue!', 'L’équipe de La Route Thermale Cycliste vous donne rendez-vous sur ses prochaines manifestations !'),
(8, 'Date', 'Prochaine Course \"LA Route Thermale Cycliste\" 1 mai 2019<br><br>Départ arrivée au centre Pré-olympique de Vittel ( CPO)');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `username`, `password`, `roles`) VALUES
(1, 'test', '$2y$10$tCMijk2JOSmvre20pPj0Fu2FP3VvVxqVBSqrmCcJm.GzMJYPx3S5i', '[\"ROLE_ADMIN\"]');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `cartes`
--
ALTER TABLE `cartes`
  ADD CONSTRAINT `FK_D8B89555BF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `circuit`
--
ALTER TABLE `circuit`
  ADD CONSTRAINT `FK_1325F3A6591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

--
-- Contraintes pour la table `contenus`
--
ALTER TABLE `contenus`
  ADD CONSTRAINT `FK_ADE120364E61B7B7` FOREIGN KEY (`page_ref_id`) REFERENCES `pages` (`id`),
  ADD CONSTRAINT `FK_ADE12036852B319B` FOREIGN KEY (`contenu_ref_id`) REFERENCES `contenus` (`id`);

--
-- Contraintes pour la table `engagement`
--
ALTER TABLE `engagement`
  ADD CONSTRAINT `FK_D86F0141591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `FK_D86F0141BF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `galeries`
--
ALTER TABLE `galeries`
  ADD CONSTRAINT `FK_EB9F215ABF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `FK_E01FBE6ABF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `FK_5E90F6D6591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `FK_5E90F6D6CF2182C8` FOREIGN KEY (`circuit_id`) REFERENCES `circuit` (`id`);

--
-- Contraintes pour la table `liens`
--
ALTER TABLE `liens`
  ADD CONSTRAINT `FK_A0A0BABCBF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `onglets`
--
ALTER TABLE `onglets`
  ADD CONSTRAINT `FK_44CDFB012EA6B36F` FOREIGN KEY (`onglet_ref_id`) REFERENCES `onglets` (`id`),
  ADD CONSTRAINT `FK_44CDFB014E61B7B7` FOREIGN KEY (`page_ref_id`) REFERENCES `pages` (`id`);

--
-- Contraintes pour la table `option_course`
--
ALTER TABLE `option_course`
  ADD CONSTRAINT `FK_55CE6D6E591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`);

--
-- Contraintes pour la table `option_inscri`
--
ALTER TABLE `option_inscri`
  ADD CONSTRAINT `FK_E1178C495DAC5993` FOREIGN KEY (`inscription_id`) REFERENCES `inscription` (`id`),
  ADD CONSTRAINT `FK_E1178C498887D912` FOREIGN KEY (`option_course_id`) REFERENCES `option_course` (`id`);

--
-- Contraintes pour la table `option_quantite`
--
ALTER TABLE `option_quantite`
  ADD CONSTRAINT `FK_AB853BDDBF396750` FOREIGN KEY (`id`) REFERENCES `option_course` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `option_simple`
--
ALTER TABLE `option_simple`
  ADD CONSTRAINT `FK_822B3FD5BF396750` FOREIGN KEY (`id`) REFERENCES `option_course` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `FK_85A59DB8BF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `textes`
--
ALTER TABLE `textes`
  ADD CONSTRAINT `FK_5C56E1D2BF396750` FOREIGN KEY (`id`) REFERENCES `contenus` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
