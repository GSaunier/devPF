<?php

namespace App\Controller;

use App\Entity\Contenus;
use App\Entity\Onglets;
use App\Entity\Pages;
use App\Entity\Parametres;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class PageController extends AbstractController
{
    /**
     * @Route("/page/{nomPage}", name="page")
     * @Route("/", name="accueil", defaults={"nomPage" = "Accueil"})
     * @Route("/erreur", name="erreur", defaults={"nomPage" = "//"})
     * @param $nomPage
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function page(string $nomPage, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $onglets = $em->getRepository(Onglets::class)->findOnglets();
        if("//" == $nomPage) {
            $page = $em->getRepository(Pages::class)->find(-1);
            if($page == null) {
                $page = new Pages();
                $page->setId(-1);
                $page->setNom("Erreur");
                $page->setType(1);
                $page->setUrl("//");
                $em->persist($page);

                $metadata = $em->getClassMetaData(get_class($page));
                $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
                $metadata->setIdGenerator(new AssignedGenerator());

                $em->flush();
            }
        } else
            $page = $em->getRepository(Pages::class)->findByUrl($nomPage);
        $param = $em->getRepository(Parametres::class)->find(0);
        $twig = $this->get('twig');
        $form = $this->get('form.factory');
        if($page == null)
             return $this->redirect('/erreur');
        $formAddContenu = $this->getForm($form, $page->getId());

        if($formAddContenu != null) {
            $this->executeForm($formAddContenu, $request, $em);
            $formAddContenu = $formAddContenu->createView();
        }

        foreach ($page->getContenus() as $c) {
            $redirect = $c->genererRendu($twig,$form,$em,$request);
            if($redirect != null)
                return $this->redirect($redirect);
        }

        return $this->render('page/index.html.twig', [
            "page" => $page,
            "onglets" => $onglets,
            "formAdd" => $formAddContenu,
            "parametres" => $param
        ]);
    }

    public function getForm(FormFactory $form,int $pageId) : ?FormInterface {
        if($this->isGranted('ROLE_ADMIN')) {
            $formAddContenu = $form->createNamedBuilder("addB");
            $formAddContenu->add("form", ChoiceType::class, ['label' => "Ajouter un contenu:", "choices" => Contenus::getListeContenu()])
                ->add("ordre", HiddenType::class, ['data' => -1])
                ->add("page", HiddenType::class, ['data' => $pageId])
                ->add("contenu", HiddenType::class, ['data' => -1])
                ->add("ajouter", SubmitType::class, ['label' => "Ajouter"]);
            return $formAddContenu->getForm();
        } else
            return null;
    }

    public function executeForm(FormInterface $form, Request $request, ObjectManager $em) {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->request->has("addB")) {
            $data = $form->getData();
            $contenu = Contenus::getNewContenu($data["form"]);
            $contenu->setOrdre($data["ordre"]);
            $contenu->setAfficher(false);
            if($data['contenu'] == -1)
                $contenu->setPageRef($em->getRepository(Pages::class)->find($data["page"]));
            else
                $contenu->setContenuRef($em->getRepository(Contenus::class)->find($data['contenu']));
            $em->getRepository(Contenus::class)->updateOrdre($contenu,true);
            $em->persist($contenu);
            $em->flush();
        }
    }
}
