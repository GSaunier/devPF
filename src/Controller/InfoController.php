<?php

namespace App\Controller;

use App\Entity\Pages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{
    /**
     * @Route("/info", name="info")
     */
    public function index()
    {
        return $this->render('info/index.html.twig', [
            'controller_name' => 'InfoController',
        ]);
    }

    /**
     * @Route("/tourl", name="tourl")
     */
    public function tourl() {
        $em = $this->getDoctrine()->getManager();
        $pages = $em->getRepository(Pages::class)->findAll();
        foreach($pages as $page) {
            $page->setUrl(urlencode($page->getNom()));
        }
        $em->flush();
        return "fait";
    }
}
