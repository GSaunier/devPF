<?php
/**
 * Created by PhpStorm.
 * User: Asus ROH
 * Date: 20/03/2019
 * Time: 21:16
 */

namespace App\Controller;


use App\Entity\Circuit;
use App\Entity\Contenus;
use App\Entity\Course;
use App\Entity\Onglets;
use App\Entity\OptionCourse;
use App\Entity\Pages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

define('MONTER',1);
define('DESCENDRE',2);
define('AFFICHER',3);
define('DELETE',0);

class AjaxController extends AbstractController
{
    /**
     * @Route("/ajax/supprContenu/{id}", name="supprC")
     */
    public function supprC(int $id) {
        if($this->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $contenu = $em->getRepository(Contenus::class)->find($id);
            $em->getRepository(Contenus::class)->updateOrdre($contenu, false);
            $em->remove($contenu);
            $em->flush();

            return new Response("true");
        } else
            return new Response("false");
    }

    /**
     * @Route("/ajax/verifNomOnglet/{nom}/{id}", name="verifNomOnglet")
     * @Route("/ajax/verifNomOnglet/{nom}", name="verifNonId", defaults={"id" = "-2"})
     */
    public function verifNomOnglet(string $nom, int $id) {
        if($this->isGranted('ROLE_ADMIN')) {
            $page = $this->getDoctrine()->getRepository(Pages::class)->findByNom($nom);
            if (!!$page) {
                return new Response("false");
            }else
                return new Response("true");

        }
        return new JsonResponse(['erreur' => "Vous n'avez pas le droit de faire ça!"],400);
    }

    /**
     * @Route("/ajax/removeInfo/{type}/{idCourse}/{idInfo}", name="removeInfo", requirements={"type" : "circuit|option"})
     */
    public function removeInfo(string $type, int $idCourse, int $idInfo)
    {
        if($this->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();

            $course = $em->getRepository(Course::class)->find($idCourse);
            if($type === "option") {
                $info = $em->getRepository(OptionCourse::class)->find($idInfo);
                $course->removeOptionCourse($info);
            } else {
                $info = $em->getRepository(Circuit::class)->find($idInfo);
                $course->removeCircuit($info);
            }
            $em->flush();
            return new Response("true");
        }
        return new JsonResponse(['erreur' => "Vous n'avez pas le droit de faire ça!"],400);
    }

    /**
     * @Route("/ajax/{type}/{id}/{action}", name="optionAction", requirements={"type" : "option|onglet"})
     */
    public function optionAction(int $id, int $action, string $type) {
        if($this->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            if($type == "option")
                $modif = $em->getRepository(Contenus::class)->find($id);
            else
                $modif = $em->getRepository(Onglets::class)->find($id);

            $ordre = $modif->getOrdre();

            if($action == MONTER)
                $modOrdre = -1;
            elseif ($action == DESCENDRE) {
                $modOrdre = 1;
                if($type == "option") {
                    if ($modif->getPageRef() != null)
                        $max = count($modif->getPageRef()->getContenus());
                    elseif ($modif->getContenuRef() != null)
                        $max = count($modif->getContenuRef()->getContenus());
                } else {
                    if($modif->getOngletRef() == null)
                        $max = count($em->getRepository(Onglets::class)->findOnglets());
                    else
                        $max = count($modif->getOngletRef()->getEnfants());
                }
            }elseif ($action == DELETE) {
                if($type == "option") {
                    $em->getRepository(Contenus::class)->updateOrdre($modif, false);
                    $modif->onDelete();
                    foreach ($modif->getContenus() as $cont) {
                        $cont->onDelete();
                        $em->remove($cont);
                    }
                } else {
                    $em->getRepository(Onglets::class)->updateOrdre($modif, false);
                    foreach ($modif->getEnfants() as $ssO)
                        $em->remove($ssO);
                }
                $em->flush();
                $em->remove($modif);
                $em->flush();
                return new Response();
            }elseif ($action == AFFICHER) {
                $modif->setAfficher(!$modif->getAfficher());
                $em->flush();
                return new JsonResponse(['rep' => $modif->getAfficher()]);
            }

            if($action == MONTER && $ordre <= 1 )
                return new JsonResponse(['erreur' => "Le contenu est déjà en haut de la page"],400);
            elseif ($action == DESCENDRE && $ordre >= $max)
                return new JsonResponse(['erreur' => "Contenu dèjà en bas de la page"],400);

            if($type == "option") {
                //Vérifie si contenu dans page ou dans multi
                if ($modif->getPageRef() != null) {
                    $em->getRepository(Contenus::class)->findByPageByOrdre($modif->getPageRef()->getId(), $ordre + $modOrdre)[0]->setOrdre($ordre);
                } elseif ($modif->getContenuRef() != null)
                    $em->getRepository(Contenus::class)->findByContenuByOrdre($modif->getContenuRef()->getId(), $ordre + $modOrdre)[0]->setOrdre($ordre);
                else
                    return new JsonResponse(['erreur' => "Le contenu n'appartient à aucune page ou contenu"]);
            } else {
                if($modif->getOngletRef() == null)
                    $em->getRepository(Onglets::class)->findByOrdre($ordre + $modOrdre)->setOrdre($ordre);
                else
                    $em->getRepository(Onglets::class)->findByOrdreByOnglet($ordre + $modOrdre,$modif->getOngletRef()->getId())->setOrdre($ordre);
            }


            $modif->setOrdre($ordre+$modOrdre);

            $em->flush();
            return new Response();
        };
        return new JsonResponse(['erreur' => "Vous n'avez pas le droit de faire ça!"],400);
    }
}