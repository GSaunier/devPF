<?php

namespace App\Controller;

use App\Entity\Onglets;
use App\Entity\Pages;
use App\Entity\Parametres;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ModificationController extends AbstractController
{


    /**
     * @Route("/parametres", name="parametres")
     */
    public function parametre(Request $request)
    {
        if(!$this->isGranted('ROLE_ADMIN'))
            return $this->redirect('/erreur');
        $em = $this->getDoctrine()->getManager();
        $page = new Pages();
        $page->setNom("Paramètres");

        $param = $em->getRepository(Parametres::class)->find(0);

        $form = $this->get('form.factory');

        $form = $param->getForm($form);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->request->has("parametres")) {
            $param->setForm($form->getData());
            $em->flush();
        }

        $onglets = $em->getRepository(Onglets::class)->findOnglets();

        return $this->render('page/parametres.html.twig', [
            "page" => $page,
            "onglets" => $onglets,
            "parametres" => $em->getRepository(Parametres::class)->find(0),
            "formAffichage" => $form->createView()
        ]);
    }
    /**
     * @Route("/modification", name="modification")
     */
    public function index(Request $request)
    {
        if(!$this->isGranted('ROLE_ADMIN'))
            return $this->redirect('/erreur');
        $em = $this->getDoctrine()->getManager();
        $page = new Pages();
        $page->setNom("Modifier Onglet");

        $form = $this->get('form.factory');

        $formAdd = $this->getFormAdd($form);
        $err[0] = $this->executeFormAdd($formAdd, $request,$em);

        $formOnglet = $this->getFormOnglet($form);
        $err[1] = $this->executeFormOnglet($formOnglet,$request,$em);

        $onglets = $em->getRepository(Onglets::class)->findOnglets();

        return $this->render('page/modOnglet.html.twig', [
            "page" => $page,
            "onglets" => $onglets,
            "formAdd" => $formAdd->createView(),
            "formOnglet" => $formOnglet,
            "modO" => true,
            "erreurs" => $err,
            "parametres" => $em->getRepository(Parametres::class)->find(0)
        ]);
    }

    public function getFormOnglet(FormFactory $form) : ?FormInterface {
        $formOnglet = $form->createNamedBuilder('modOnglet');
        $formOnglet->add('nom', TextType::class, ['label' => "Nom"])
            ->add('id', HiddenType::class, ['data' => -1])
            ->add('idPage', HiddenType::class, ['data' => -1])
            ->add("modifier", SubmitType::class, ['label' => "Modifier", "attr" => ["class" => "col s8 offset-s2"] ]);
        return $formOnglet->getForm();
    }

    public function executeFormOnglet(FormInterface $form, Request $request, ObjectManager $em): ?string {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->request->has("modOnglet")) {
            $data = $form->getData();
            $test = $em->getRepository(Pages::class)->findByNom($data['nom']);
            if(!!$test)
                return "Le nom de l'onglet existe déjà!";
            $onglet = $em->getRepository(Onglets::class)->find($data['id']);
            $onglet->setNom($data['nom']);
            $onglet->getPageRef()->setNom($data['nom'])->updateUrl();
            $em->flush();
        }
        return null;
    }

    public function getFormAdd(FormFactory $form) : ?FormInterface {
            $formAddContenu = $form->createNamedBuilder("addB");
            $formAddContenu->add("form", TextType::class, ['label' => 'Nom onglet :'])
                ->add("ordre", HiddenType::class, ['data' => -1])
                ->add('ongletRef', HiddenType::class, ['data' => -1])
                ->add("ajouter", SubmitType::class, ['label' => "Ajouter"]);
            return $formAddContenu->getForm();
    }

    public function executeFormAdd(FormInterface $form, Request $request, ObjectManager $em): ?string {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->request->has("addB")) {
            $data = $form->getData();
            if(!!($em->getRepository(Pages::class)->findByNom($data['form'])))
                return "Le nom de l'onglet existe déjà!";
            $onglet = new Onglets();
            $page = new Pages();
            $page->setNom($data['form']);
            $page->setUrl(urlencode($page->getNom()));
            $page->setType(1);
            $onglet->setPageRef($page);
            $onglet->setOrdre($data["ordre"]);
            $onglet->setAfficher(false);
            $onglet->setNom($data['form']);
            if($data['ongletRef'] != -1)
                $onglet->setOngletRef($em->getRepository(Onglets::class)->find($data['ongletRef']));
            $em->getRepository(Onglets::class)->updateOrdre($onglet,true);
            $em->persist($onglet);
            $em->flush();
        }
        return null;
    }
}
