<?php

namespace App\Controller;


use App\Entity\Onglets;
use App\Entity\Pages;
use App\Entity\Parametres;
use App\Entity\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ConnexionController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $em = $this->getDoctrine()->getManager();
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();


        $onglets = $this->getDoctrine()->getRepository(Onglets::class)->findOnglets();
        $page = new Pages();
        $page->setNom("Connexion");
        return $this->render('connexion/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'page'          => $page,
            'onglets'       => $onglets,
            "parametres" => $em->getRepository(Parametres::class)->find(0)
        ));
    }


    /**
     * @Route("/register", name="user_registration")
     */

    public function registerAction(Request $request)
    {
        $user = new Utilisateur();
        $form = $this->get('form.factory')->createNamedBuilder('form', FormType::class, $user)
            ->add('username',TextType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(password_hash($user->getPassword(),PASSWORD_BCRYPT));
            // Par defaut l'utilisateur aura toujours le rôle ROLE_USER
                $user->setRoles("[\"ROLE_ADMIN\"]");

            // On enregistre l'utilisateur dans la base
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'connexion/register.html.twig',
            array('form' => $form->createView())
        );
    }




}