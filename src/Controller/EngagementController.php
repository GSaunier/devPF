<?php

namespace App\Controller;

use App\Entity\Engagement;
use App\Entity\Inscription;
use App\Entity\Onglets;
use App\Entity\Pages;
use App\Entity\Parametres;
use App\Paiement;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Exception\PayPalConnectionException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EngagementController extends AbstractController
{
    /**
     * @Route("/engagement/{idEng}/{idInscrit}/true", name="engaPayer")
     */
    public function payer($idEng, $idInscrit)
    {
        $paymentId = $_GET['paymentId'];
        $apiContext = Paiement::getContexte();

        $payment = Payment::get($paymentId, $apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($_GET['PayerID']);

        try {
            $result = $payment->execute($execution, $apiContext);
        } catch (PayPalConnectionException $ex) {
            return $this->redirect($this->annuler($idEng,$idInscrit));
        }

        $em = $this->getDoctrine()->getManager();
        $onglets = $em->getRepository(Onglets::class)->findOnglets();

        $eng = $em->getRepository(Engagement::class)->find($idEng);

        $page = $em->getRepository(Pages::class)->find(3);

        $inscri = $em->getRepository(Inscription::class)->find($idInscrit);

        $inscri->setPayer(true);

        $param = $em->getRepository(Parametres::class)->find(0);

        $em->flush();

        return $this->render('engagement/index.html.twig', [
            "page" => $page,
            "onglets" => $onglets,
            "parametres" => $param
        ]);
    }


    /**
     * @Route("/engagement/{idEng}/{idInscrit}/false", name="annuler")
     */
    public function annuler($idEng, $idInscrit)
    {
        $em = $this->getDoctrine()->getManager();
        $onglets = $em->getRepository(Onglets::class)->findOnglets();

        $param = $em->getRepository(Parametres::class)->find(0);

        $eng = $em->getRepository(Engagement::class)->find($idEng);

        $page = $em->getRepository(Pages::class)->find(3);

        $inscri = $em->getRepository(Inscription::class)->find($idInscrit);

        $em->remove($inscri);
        $em->flush();

        return $this->render('engagement/annulation.html.twig', [
            "page" => $page,
            "onglets" => $onglets,
            "parametres" => $param
        ]);
    }
}
