<?php

namespace App\Controller;

use App\Entity\Parametres;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    /**
     * @Route("/stylesheet", name="styleSheet")
     */
    public function styleSheet() {

        $response = $this->render('css/style.css.twig', [
            "parametres" => $this->getDoctrine()->getManager()->getRepository(Parametres::class)->find(0),
        ]);

        $response->headers->set('Content-Type', 'text/css');

        return $response;
    }
}
