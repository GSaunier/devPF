<?php

namespace App\Controller;

use App\Entity\Liens;
use App\Entity\Pages;
use App\Entity\PiecesJointes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    /**
     * @Route("/test", name="test")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Pages $page
         */
        $page = $em->getRepository(Pages::class)->find(2);

        $pj = new Liens();
        $pj->setNom("Parcours et profil");
        $pj->setCommentaire("Bonjour à tous, vous trouverez le lien précisant le règlement de notre épreuve");
        $pj->setChemin("Images/pieceJointes/Réglement-Course-2018.pdf");
        $pj->setOrdre(2);
        $pj->setPj(true);

        $page->addContenus($pj);

        $em->persist($pj);
        $em->flush();
        return $this->render('test/index.html.twig', [
            'controller_name' => 'TestController',
        ]);
    }
}
