<?php
/**
 * Created by PhpStorm.
 * User: Asus ROH
 * Date: 26/02/2019
 * Time: 13:27
 */

namespace App;


use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Symfony\Component\Dotenv\Dotenv;

class Paiement
{
    static public function getContexte() {
        $dotenv = new Dotenv();
        $dotenv->load('../.env');

        $clientid = getenv('CLIENTID');
        $clientsecret = getenv('CLIENTSECRET');

        $apiContext = new ApiContext(new OAuthTokenCredential($clientid,$clientsecret) );
        $apiContext->setConfig(array('mode' => 'live'));

        return $apiContext;
    }


    static public function getBaseURL() {
        $dotenv = new Dotenv();
        $dotenv->load('../.env');

        return getenv('BASEURL');
    }
}