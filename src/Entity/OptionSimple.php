<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use PayPal\Api\Item;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionSimpleRepository")
 */
class OptionSimple extends OptionCourse
{

    /**
     * @ORM\Column(type="boolean")
     * @ORM\JoinColumn(nullable=true)
     */
    private $choix;

    public function getChoix(): ?bool
    {
        return $this->choix;
    }

    public function setChoix(bool $choix): self
    {
        $this->choix = $choix;

        return $this;
    }

    public function calculer(): int
    {
        return $this->prix;
    }

    public function getLabel(): string
    {
        return $this->titre.' Prix: '.$this->prix.' €';
    }


    public function getItem(): Item
    {
        $item = new Item();
        $item->setName($this->titre)
            ->setCurrency('EUR')
            ->setPrice($this->prix);
        return $item;
    }

    public function getType(): ?string
    {
        return "Simple";
    }
}
