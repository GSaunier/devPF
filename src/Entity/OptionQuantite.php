<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use PayPal\Api\Item;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionQuantiteRepository")
 */
class OptionQuantite extends OptionCourse
{
    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\Column(type="integer")
     */
    private $max;

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function calculer(): int {
        return $this->prix*$this->quantite;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(int $max): self
    {
        $this->max = $max;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->titre.' Prix: '.$this->prix.' €/unité';
    }

    public function getItem(): Item
    {
        $item = new Item();
        $item->setName($this->titre)
            ->setCurrency('EUR')
            ->setPrice($this->prix);
        return $item;
    }

    public function getType(): ?string
    {
        return "Quantite";
    }
}
