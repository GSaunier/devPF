<?php

namespace App\Entity;

use App\Paiement;
use App\Repository\CircuitRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use PayPal\Api\Amount;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EngagementRepository")
 */
class Engagement extends Contenus
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Course", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @var Course $course
     */
    private $course;

    public function getType()
    {
        return "engagement";
    }


    public function getFormInscription(FormFactory $form) : ?FormInterface
    {
        $idCourse = $this->course->getId();
        $tabCir = null;
        foreach ($this->course->getCircuits() as $circuit ) {
            $tabCir[$circuit->getNom().' Prix: '.$circuit->getPrix().' €'] = $circuit->getId();
        }
        $form = $form->createNamedBuilder("engagement");
        $form->add('nom', TextType::class, ['label' => 'Nom:', 'attr' => ['data-length' => 30, 'maxlength' => 30, 'class' => "text-input"]])
            ->add('prenom',TextType::class,['label' => 'Prénom:', 'attr' => ['data-length' => 30, 'maxlength' => 30, 'class' => "text-input"]])
            ->add('dateNaissance',DateType::class, ['widget' => 'single_text','label' => 'Date de naissance:','format' => 'dd-MM-yyyy'])
            ->add('categorie', ChoiceType::class, ['choices' => ['Homme' => 'Homme', 'Femme' => 'Femme', 'Handisport' => 'Handisport']])
            ->add('club', TextType::class, ['label' => 'Club:', 'attr' => ['data-length' => 60, 'maxlength' => 60, 'class' => "text-input"], 'required' => false])
            ->add('licenceCertif', TextType::class,['label' => 'Num licence / date certif. méd:', 'attr' => ['data-length' => 50, 'maxlength' => 50, 'class' => "text-input"]])
            ->add('adresse', TextType::class, ['label' => "Adresse", 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add('codePostal',TextType::class, ['label' => "Code postal:", 'attr' => ['data-length' => 10, 'maxlength' => 10, 'class' => "text-input"]])
            ->add('ville',TextType::class, ['label' => "Ville:", 'attr' => ['data-length' => 30, 'maxlength' => 30, 'class' => "text-input"]])
            ->add('tel', TextType::class, ['label' => 'Numéro de téléphone:', 'attr' => ['data-length' => 16, 'maxlength' => 16, 'class' => "text-input"]])
            ->add('mail', EmailType::class, ['label' => 'Email:', 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add('circuit', EntityType::class, ['class' => Circuit::class, 'choice_label' => 'label' ,'query_builder' => function(CircuitRepository $er) use ($idCourse) {
                return$er->queryFindByCourse($idCourse);
            },
                'label' => 'Choix du circuit:']);
        foreach ($this->course->getOptionCourses() as $option) {
            if ($option instanceof OptionSimple)
                $form->add('option:'.$option->getId(), CheckboxType::class, ['label' => $option->getLabel(),'required' => false, 'attr' => ['valeur' => $option->getId()]]);
            else {
                $form->add('option:'.$option->getId(), IntegerType::class, ['label' => $option->getLabel(), 'required' => false, 'attr' => ['valeur' => $option->getId(), 'min' => 0, 'max' => 10]]);
            }
        }
        $form->add('valider',SubmitType::class,['label' => "Valider"]);

        return $form->getForm();
    }

    public function genererPaiement(Inscription $inscri) :?string {


        $apiContext = Paiement::getContexte();

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');


        $items = array();
        $items[0] = $inscri->getCircuit()->getItem();
        $i = 1;
        foreach ($inscri->getOptionInscris() as $option) {
            $items[$i] = $option->getItem();
            $i++;
        }

        $itemList = new ItemList();


        $total = 0;
        for ($j = 0; $j < $i; $j++) {
            $total += floatval($items[$j]->getPrice()) * intval($items[$j]->getQuantity());
            $itemList->addItem($items[$j]);
        }

        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($total);

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setItemList($itemList)
            ->setDescription("Paiement route thermale cycliste.")
            ->setInvoiceNumber(uniqid());


        $baseURL = Paiement::getBaseURL();
        $redirectURL = new RedirectUrls();
        $redirectURL->setReturnUrl($baseURL.'/engagement/'.$this->course->getId().'/'.$inscri->getId().'/true')
            ->setCancelUrl($baseURL.'/engagement/'.$this->course->getId().'/'.$inscri->getId().'/false');

        $payement = new Payment();
        $payement->setIntent('sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectURL)
            ->setTransactions(array($transaction))
            ->create($apiContext);

        return $payement->getApprovalLink();

    }

    public function executeFormInscription(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        /** @var Inscription $inscri */
        $inscri = new Inscription();

        $inscri->setNom($data['nom'])
            ->setPrenom($data['prenom'])
            ->setDateNaissance($data['dateNaissance'])
            ->setCategorie($data['categorie'])
            ->setClub($data['club'])
            ->setLicenceCertif($data['licenceCertif'])
            ->setAdresse($data['adresse'])
            ->setCodePostal($data['codePostal'])
            ->setVille($data['ville'])
            ->setTelephone($data['tel'])
            ->setMail($data['mail'])
            ->setCourse($this->course)
            ->setCircuit($data['circuit'])
            ->setPayer(false)
        ;

        foreach ($data as $key => $value){
            if (substr($key,0,7) == 'option:') {
                if ($value != false && $value != '0'){
                    if ($value === true) {
                        $value = 1;
                    }
                    $id = substr($key,7);
                    $option = new OptionInscri();
                    $option->setInscription($inscri)
                        ->setOptionCourse($em->getRepository(OptionCourse::class)->find($id))
                        ->setQuantite($value);
                    $inscri->addOptionInscri($option);
                }
            }
        }

        $em->persist($inscri);
        $em->flush();

        return $this->genererPaiement($inscri);
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form,ObjectManager $em, Request $request) : ?string {
        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("engagement".$this->getId())) {
            $this->executeForm($f,$em);
        }
        $formInscription = $this->getFormInscription($form);
        $formInscription->handleRequest($request);

        if ($formInscription->isSubmitted() && $formInscription->isValid() && $request->request->has("engagement")) {
            return $this->executeFormInscription($formInscription, $em);
        }

        $formCircuit = $this->createFormCircuit($form);
        $formCircuit->handleRequest($request);

        if ($formCircuit->isSubmitted() && $formCircuit->isValid() && $request->request->has("engagementCircuit".$this->getId())) {
            $this->executeFormCircuit($formCircuit, $em);
        }

        $formOption = $this->createFormOption($form);
        $formOption->handleRequest($request);

        if ($formOption->isSubmitted() && $formOption->isValid() && $request->request->has("engagementOption".$this->getId())) {
            $this->executeFormOption($formOption, $em);
        }

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "formInscription" => $formInscription->createView(),
            "formCircuit" => $formCircuit->createView(),
            "formOption" => $formOption->createView(),
            "f" => $f->createView()
        ]);
        return null;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function createFormCircuit(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("engagementCircuit".$this->getId());
        $form->add("courseId", HiddenType::class, ['data' => $this->course->getId()])
            ->add("distance", IntegerType::class, ['label' => "Distance", 'attr' => ['min' => 0, "max" => 500]])
            ->add("nom", TextType::class, ['label' => "nom", 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add("prix", NumberType::class, ['label' => "Prix", 'scale' => 2, "attr" => ["min" => 0, "step" => 0.01,"class" => "jsReplace"]])
            ->add("submit", SubmitType::class, ['label' => "Ajouter"]);
        return $form->getForm();
    }

    public function createFormOption(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("engagementOption".$this->getId());
        $form->add("courseId", HiddenType::class, ['data' => $this->course->getId()])
            ->add("titre", TextType::class, ['label' => "nom", 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add("prix",NumberType::class, ['label' => "Prix", 'scale' => 2 ,"attr" => ["min" => 0, "step" => 0.01, "class" => "jsReplace"]])
            ->add("quantite", CheckboxType::class, ['label' => "Quantité", "required" => false])
            ->add("submit", SubmitType::class, ['label' => "Ajouter"]);
        return $form->getForm();
    }

    public function  executeFormOption(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        if($data['quantite']){
            $option = new OptionQuantite();
            $option->setQuantite(0);
            $option->setMax(10);
        } else {
            $option = new OptionSimple();
            $option->setChoix(false);
        }

        $option->setPrix($data['prix'])
            ->setTitre($data['titre']);

        $this->course->addOptionCourse($option);

        $em->persist($option);
        $em->flush();
    }

    public function executeFormCircuit(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        $circuit = new Circuit();
        $circuit->setNom($data['nom'])
            ->setDistance($data['distance'])
            ->setPrix($data['prix']);
        $this->course->addCircuit($circuit);
        $em->persist($circuit);
        $em->flush();
    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("engagement".$this->getId());
        $form->add("comm", CKEditorType::class, ['data' => $this->course->getDescription(), 'label' => false])
            ->add("titre", TextType::class, ['data' => $this->course->getNom(), 'label' => false, 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        $this->course->setDescription($data['comm']);
        if (is_null($this->getContenuRef()))
            $this->course->setNom($data['titre']);
        //$this->setAfficher($data["afficher"]);

        $em->flush();
    }
}
