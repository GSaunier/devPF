<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PayPal\Api\Item;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionCourseRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"simple" = "OptionSimple", "quantite" = "OptionQuantite"})
 */
abstract class OptionCourse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $titre;

    /**
     * @ORM\Column(type="float")
     */
    protected $prix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="optionCourses")
     * @ORM\JoinColumn(nullable=true)
     */
    private $course;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OptionInscri", mappedBy="optionCourse", orphanRemoval=true)
     */
    private $optionInscris;

    public function __construct()
    {
        $this->inscriptions = new ArrayCollection();
        $this->optionInscris = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    abstract public function calculer(): int ;
    abstract public function getLabel(): string;
    abstract public function getItem(): Item;

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->addOption($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            $inscription->removeOption($this);
        }

        return $this;
    }

    /**
     * @return Collection|OptionInscri[]
     */
    public function getOptionInscris(): Collection
    {
        return $this->optionInscris;
    }

    public function addOptionInscri(OptionInscri $optionInscri): self
    {
        if (!$this->optionInscris->contains($optionInscri)) {
            $this->optionInscris[] = $optionInscri;
            $optionInscri->setOptionCourse($this);
        }

        return $this;
    }

    public function removeOptionInscri(OptionInscri $optionInscri): self
    {
        if ($this->optionInscris->contains($optionInscri)) {
            $this->optionInscris->removeElement($optionInscri);
            // set the owning side to null (unless already changed)
            if ($optionInscri->getOptionCourse() === $this) {
                $optionInscri->setOptionCourse(null);
            }
        }

        return $this;
    }

    public abstract function getType(): ?string;
}
