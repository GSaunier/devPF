<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InscriptionRepository")
 */
class Inscription
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $prenom;

    /**
     * @ORM\Column(type="date")
     */
    private $dateNaissance;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $club;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $licenceCertif;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ville;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Circuit", inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $circuit;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $telephone;

    /**
     * @ORM\Column(type="boolean")
     */
    private $payer;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $mail;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Course", inversedBy="inscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $course;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OptionInscri", mappedBy="inscription", orphanRemoval=true, cascade={"persist"})
     */
    private $optionInscris;

    public function __construct()
    {
        $this->optionInscris = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getClub(): ?string
    {
        return $this->club;
    }

    public function setClub(?string $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getLicenceCertif(): ?string
    {
        return $this->licenceCertif;
    }

    public function setLicenceCertif(string $licenceCertif): self
    {
        $this->licenceCertif = $licenceCertif;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCircuit(): ?Circuit
    {
        return $this->circuit;
    }

    public function setCircuit(?Circuit $circuit): self
    {
        $this->circuit = $circuit;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPayer(): ?bool
    {
        return $this->payer;
    }

    public function setPayer(bool $payer): self
    {
        $this->payer = $payer;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    /**
     * @return Collection|OptionInscri[]
     */
    public function getOptionInscris(): Collection
    {
        return $this->optionInscris;
    }

    public function addOptionInscri(OptionInscri $optionInscri): self
    {
        if (!$this->optionInscris->contains($optionInscri)) {
            $this->optionInscris[] = $optionInscri;
            $optionInscri->setInscription($this);
        }

        return $this;
    }

    public function removeOptionInscri(OptionInscri $optionInscri): self
    {
        if ($this->optionInscris->contains($optionInscri)) {
            $this->optionInscris->removeElement($optionInscri);
            // set the owning side to null (unless already changed)
            if ($optionInscri->getInscription() === $this) {
                $optionInscri->setInscription(null);
            }
        }

        return $this;
    }

    public function getPrix() {
        $total = $this->circuit->getPrix();
        /** @var OptionInscri $option */
        foreach ($this->optionInscris as $option) {
            $total += floatval($option->getItem()->getPrice())*$option->getQuantite();
        }
        return $total;
    }
}
