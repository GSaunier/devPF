<?php

namespace App\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MultiRepository")
 */
class Multi extends Contenus
{
    public function getType()
    {
        return 'multi';
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form, ObjectManager $em, Request $request): ?string
    {
        foreach ($this->getContenus() as $c) {
            $c->genererRendu($twig,$form,$em,$request);
        }

        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("multi".$this->getId())) {
            $this->executeForm($f,$em);
        }

        $param = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "f" => $f->createView(),
            "p" => $param
        ]);
        return null;

    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("multi".$this->getId());
        $form->add("titre", TextType::class, ['data' => $this->getTitre(), 'label' => false, 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        $this->setTitre($data['titre']);
        $em->flush();
    }
}
