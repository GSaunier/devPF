<?php

namespace App\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CartesRepository")
 */
class Cartes extends Contenus
{
    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $commentaire;

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getType()
    {
        return "carte";
    }

    public function createForm(FormFactory $form): FormInterface
    {
        // TODO: Implement addRowForm() method.
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        // TODO: Implement executeForm() method.
    }
}
