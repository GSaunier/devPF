<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourseRepository")
 */
class Course
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOuverture;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateFermeture;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Circuit", mappedBy="course", orphanRemoval=true)
     */
    private $circuits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OptionCourse", mappedBy="course", orphanRemoval=true)
     */
    private $optionCourses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Inscription", mappedBy="course", orphanRemoval=true)
     */
    private $inscriptions;

    public function __construct()
    {
        $this->circuits = new ArrayCollection();
        $this->optionCourses = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateOuverture(): ?\DateTimeInterface
    {
        return $this->dateOuverture;
    }

    public function setDateOuverture(?\DateTimeInterface $dateOuverture): self
    {
        $this->dateOuverture = $dateOuverture;

        return $this;
    }

    public function getDateFermeture(): ?\DateTimeInterface
    {
        return $this->dateFermeture;
    }

    public function setDateFermeture(?\DateTimeInterface $dateFermeture): self
    {
        $this->dateFermeture = $dateFermeture;

        return $this;
    }

    /**
     * @return Collection|Circuit[]
     */
    public function getCircuits(): Collection
    {
        return $this->circuits;
    }

    public function addCircuit(Circuit $circuit): self
    {
        if (!$this->circuits->contains($circuit)) {
            $this->circuits[] = $circuit;
            $circuit->setCourse($this);
        }

        return $this;
    }

    public function removeCircuit(Circuit $circuit): self
    {
        if ($this->circuits->contains($circuit)) {
            $this->circuits->removeElement($circuit);
            // set the owning side to null (unless already changed)
            if ($circuit->getCourse() === $this) {
                $circuit->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OptionCourse[]
     */
    public function getOptionCourses(): Collection
    {
        return $this->optionCourses;
    }

    public function addOptionCourse(OptionCourse $optionCourse): self
    {
        if (!$this->optionCourses->contains($optionCourse)) {
            $this->optionCourses[] = $optionCourse;
            $optionCourse->setCourse($this);
        }

        return $this;
    }

    public function removeOptionCourse(OptionCourse $optionCourse): self
    {
        if ($this->optionCourses->contains($optionCourse)) {
            $this->optionCourses->removeElement($optionCourse);
            // set the owning side to null (unless already changed)
            if ($optionCourse->getCourse() === $this) {
                $optionCourse->setCourse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Inscription[]
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    public function addInscription(Inscription $inscription): self
    {
        if (!$this->inscriptions->contains($inscription)) {
            $this->inscriptions[] = $inscription;
            $inscription->setCourse($this);
        }

        return $this;
    }

    public function removeInscription(Inscription $inscription): self
    {
        if ($this->inscriptions->contains($inscription)) {
            $this->inscriptions->removeElement($inscription);
            // set the owning side to null (unless already changed)
            if ($inscription->getCourse() === $this) {
                $inscription->setCourse(null);
            }
        }

        return $this;
    }
}
