<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PagesRepository")
 */
class Pages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Onglets", mappedBy="pageRef", cascade={"persist", "remove"})
     */
    private $onglet;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contenus", mappedBy="pageRef", orphanRemoval=true)
     * @ORM\OrderBy({"ordre" = "ASC"})
     */
    private $contenus;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function __construct()
    {
        $this->contenus = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOnglet(): ?Onglets
    {
        return $this->onglet;
    }

    public function setOnglet(?Onglets $onglet): self
    {
        $this->onglet = $onglet;

        // set (or unset) the owning side of the relation if necessary
        $newPageRef = $onglet === null ? null : $this;
        if ($newPageRef !== $onglet->getPageRef()) {
            $onglet->setPageRef($newPageRef);
        }

        return $this;
    }

    public function updateUrl() {
        $this->url = urlencode($this->nom);
    }

    /**
     * @return Collection|Contenus[]
     */
    public function getContenus(): Collection
    {
        return $this->contenus;
    }

    public function addContenus(Contenus $contenus): self
    {
        if (!$this->contenus->contains($contenus)) {
            $this->contenus[] = $contenus;
            $contenus->setPageRef($this);
        }

        return $this;
    }

    public function removeContenus(Contenus $contenus): self
    {
        if ($this->contenus->contains($contenus)) {
            $this->contenus->removeElement($contenus);
            // set the owning side to null (unless already changed)
            if ($contenus->getPageRef() === $this) {
                $contenus->setPageRef(null);
            }
        }

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
