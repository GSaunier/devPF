<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OngletsRepository")
 */
class Onglets
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nom;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Pages", inversedBy="onglet", cascade={"persist", "remove"})
     */
    private $pageRef;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Onglets", mappedBy="ongletRef")
     * @ORM\OrderBy({"ordre" = "ASC"})
     */
    private $enfants;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Onglets", inversedBy="enfants")
     */
    private $ongletRef;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $ordre;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $afficher;

    public function __construct()
    {
        $this->enfants = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPageRef(): ?Pages
    {
        return $this->pageRef;
    }

    public function setPageRef(?Pages $pageRef): self
    {
        $this->pageRef = $pageRef;

        return $this;
    }

    /**
     * @return Collection|Onglets[]
     */
    public function getEnfants(): Collection
    {
        return $this->enfants;
    }

    public function addEnfant(Onglets $enfant): self
    {
        if (!$this->enfants->contains($enfant)) {
            $this->enfants[] = $enfant;
            $enfant->setOngletRef($this);
        }

        return $this;
    }

    public function removeEnfant(Onglets $enfant): self
    {
        if ($this->enfants->contains($enfant)) {
            $this->enfants->removeElement($enfant);
            // set the owning side to null (unless already changed)
            if ($enfant->getOngletRef() === $this) {
                $enfant->setOngletRef(null);
            }
        }

        return $this;
    }

    public function getOngletRef(): ?self
    {
        return $this->ongletRef;
    }

    public function setOngletRef(?self $ongletRef): self
    {
        $this->ongletRef = $ongletRef;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getAfficher(): ?bool
    {
        return $this->afficher;
    }

    public function setAfficher(bool $afficher): self
    {
        $this->afficher = $afficher;

        return $this;
    }
}
