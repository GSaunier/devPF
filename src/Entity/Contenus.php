<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContenusRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"engagement" = "Engagement", "paretenaires" = "Partenaires", "galeries" = "Galeries", "sliders" = "Sliders", "cartes" = "Cartes", "textes" = "Textes", "images" = "Images", "liens" = "Liens", "multi" = "Multi"})
 */
abstract class Contenus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Pages", inversedBy="contenus")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pageRef;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $ordre;

    /**
     * @var $html string
     */
    protected $html;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contenus", inversedBy="contenuses")
     */
    private $contenuRef;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Contenus", mappedBy="contenuRef")
     * @ORM\OrderBy({"ordre" = "ASC"})
     */
    private $contenuses;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $afficher;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $titre;

    public function __construct()
    {
        $this->contenuses = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPageRef(): ?Pages
    {
        return $this->pageRef;
    }

    public function setPageRef(?Pages $pageRef): self
    {
        $this->pageRef = $pageRef;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    abstract public function createForm(FormFactory $form): FormInterface;

    public function genererRendu(\Twig_Environment $twig, FormFactory $form,ObjectManager $em, Request $request) : ?string
    {
        $p = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/'.$this->getType().
            '.html.twig', [
            "t" => $this,
            "p" => $p,
            "f" => $form->createNamedBuilder("test")->getForm()->createView()
        ]);

        return null;
    }

    public function getHtml()
    {
        return $this->html;
    }

    abstract public function getType();

    abstract public function executeForm(FormInterface $form, ObjectManager $em);

    public function getContenuRef(): ?self
    {
        return $this->contenuRef;
    }

    public function setContenuRef(?self $contenuRef): self
    {
        $this->contenuRef = $contenuRef;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getContenus(): Collection
    {
        return $this->contenuses;
    }

    public function addContenus(self $contenus): self
    {
        if (!$this->contenuses->contains($contenus)) {
            $this->contenuses[] = $contenus;
            $contenus->setContenuRef($this);
        }

        return $this;
    }

    public function onDelete() {

    }

    public function removeContenus(self $contenus): self
    {
        if ($this->contenuses->contains($contenus)) {
            $this->contenuses->removeElement($contenus);
            // set the owning side to null (unless already changed)
            if ($contenus->getContenuRef() === $this) {
                $contenus->setContenuRef(null);
            }
        }

        return $this;
    }

    public static function getListeContenu() : array {
        $array = ["Partenaires" => "Partenaires", "Galeries" => "Galeries", "Sliders" => "Sliders", "Textes" => "Textes", "Images" => "Images", "Liens" => "Liens", "Engagement" => "Engagement"];
        return $array;
    }

    public static function getNewContenu(string $type): ?Contenus {
        switch ($type) {
            case "Engagement":
                $course = new Course();
                $engagement = new Engagement();
                $engagement->setCourse($course);
                return $engagement;
            case "Galeries":
                return new Galeries();
            case "Sliders":
                return new Sliders();
            case "Cartes":
                return new Cartes();
            case "Textes":
                return new Textes();
            case "Images":
                return new Images();
            case "Liens":
                return new Liens();
            case "Multi":
                return new Multi();
            case "Partenaires":
                return new Partenaires();
            default:
                return null;
        }
    }

    public function getAfficher(): ?bool
    {
        return $this->afficher;
    }

    public function setAfficher(bool $afficher): self
    {
        $this->afficher = $afficher;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }
}
