<?php

namespace App\Entity;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TextesRepository")
 */
class Textes extends Contenus
{
    /**
     * @ORM\Column(type="string", length=10000, nullable=true)
     */
    private $commentaire;

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire($commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        $this->setCommentaire($data["comm"]);
        if (is_null($this->getContenuRef()))
            $this->setTitre($data['titre']);
        //$this->setAfficher($data["afficher"]);

        $em->flush();
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form, ObjectManager $em, Request $request): ?string
    {
        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("texte".$this->getId())) {
            $this->executeForm($f,$em);
        }

        $param = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "f" => $f->createView(),
            "p" => $param
        ]);
        return null;

    }

    public function getType()
    {
        return "texte";
    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("texte".$this->getId());
        $form->add("comm", CKEditorType::class, ['data' => $this->getCommentaire(), 'label' => false]);
        if (is_null($this->getContenuRef()))
            $form->add("titre", TextType::class, ['data' => $this->getTitre(), 'label' => false, 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]]);
        $form->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }
}
