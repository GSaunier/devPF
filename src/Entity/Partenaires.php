<?php

namespace App\Entity;

use App\Utilitaire;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartenairesRepository")
 */
class Partenaires extends Contenus
{
    public function getImagesPath(): array
    {
        $path = [];
        try {
            $dossier = opendir("Images/Partenaires/".$this->getId());
        } catch (\Exception $e) {
            return $path;
        }
        while (false !== ($fichier = readdir($dossier))) {
            if($fichier != '.' && $fichier != '..' ) {
                $path[] = '/Images/Partenaires/' . $this->getId() . '/' . $fichier;
            }
        }
        return $path;
    }

    public function getNbPages() {
        try {
            $path = 'Images/Partenaires/' . $this->getId();
            if(!file_exists('Images/Partenaires/' . $this->getId()))
                mkdir($path);
            $files = array_diff(scandir($path), array('..', '.'));
        } catch (\Exception $e) {
            return 1;
        }
        $nbPage = intval(sizeof($files)/10);
        if(sizeof($files)%10 != 0)
            $nbPage++;
        return $nbPage;
    }

    public function getType()
    {
        return "partenaires";
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form, ObjectManager $em, Request $request): ?string
    {
        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("texte".$this->getId())) {
            $this->executeForm($f,$em);
        }

        $param = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "f" => $f->createView(),
            "p" => $param
        ]);
        return null;

    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("texte".$this->getId());
        $form->add("titre", TextType::class, ['data' => $this->getTitre(), 'label' => false, 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add('img', FileType::class, ['label' => 'Image', 'multiple' => true,'attr' => [
                'accept' => 'image/*',
                'multiple' => 'multiple'
            ]])
            ->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        /** @var UploadedFile[] $files */
        $files = $data['img'];
        $this->setTitre($data['titre']);
        if (sizeof($files) != 0){
            array_map('unlink', glob("Images/Partenaires/". $this->getId() . '/*'));
            foreach ($files as $file){
                $file->move("Images/Partenaires/" . $this->getId() . "/", $file->getClientOriginalName());
            }
        }
        $em->flush();
    }

    public function onDelete()
    {
        Utilitaire::deleteDirectory("Images/Partenaires/" . $this->getId());
    }
}
