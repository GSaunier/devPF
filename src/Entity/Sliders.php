<?php

namespace App\Entity;

use App\Utilitaire;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SlidersRepository")
 */
class Sliders extends Contenus
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getImg(): array{
        $path = 'Images/Slider/' . $this->getId();
        if(!file_exists($path))
            return [];
        $files = array_diff(scandir($path), array('..', '.'));
        for ($i = 2; $i < 2+ sizeof($files) ; $i++)
            $files[$i] = '/Images/Slider/' . $this->getId() . '/' . $files[$i];
        return $files;
    }

    public function getNombreImages(): ?int
    {
        return $this->getContenus()->count();
    }

    public function getType()
    {
        return "slider";
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form, ObjectManager $em, Request $request): ?string
    {
        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("texte".$this->getId())) {
            $this->executeForm($f,$em);
        }

        $param = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "f" => $f->createView(),
            "p" => $param
        ]);
        return null;

    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("texte".$this->getId());
        $form->add('img', FileType::class, ['label' => 'Image', 'multiple' => true,'attr' => [
            'accept' => 'image/*',
            'multiple' => 'multiple',
            'maxSize' => '40M'
        ]])
            ->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        /** @var UploadedFile[] $files */
        $files = $data['img'];
        if (sizeof($files) != 0) {
            array_map('unlink', glob("Images/Slider/" . $this->getId() . '/*'));
            foreach ($files as $file) {
                $file->move("Images/Slider/" . $this->getId() . "/", $file->getClientOriginalName());
            }
        }
    }

    public function onDelete()
    {
        Utilitaire::deleteDirectory("Images/Slider/" . $this->getId());
    }
}
