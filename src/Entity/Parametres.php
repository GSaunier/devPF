<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParametresRepository")
 */
class Parametres
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $couleur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $footerInfo;

    public function setForm(array $data)
    {
        /** @var UploadedFile $logo */
        $logo = $data['logo'];

        if(!is_null($logo))
            $logo->move("Images/Parametres", "logo.png");

        /** @var UploadedFile $fond */
        $fond = $data['fond'];
        if(!is_null($fond))
            $fond->move("Images/Parametres", "fond.png");

        /** @var UploadedFile $imageTitre */
        $imageTitre = $data['titre'];
        if(!is_null($imageTitre))
            $imageTitre->move("Images/Parametres", "titre.png");

        $this->titre = $data['nom'];
        $this->footerInfo = $data['description'];
        $this->couleur = $data['couleur'];
    }

    public function getForm(FormFactory $form): FormInterface {
        $form = $form->createNamedBuilder("parametres");
        $form->add("fond",FileType::class, ['label' => "Image de fond", 'required' => false])
            ->add("logo", FileType::class, ['label' => "Logo", 'required' => false])
            ->add("titre", FileType::class, ['label' => "Image du titre", 'required' => false])
            ->add("nom", TextType::class, ['label' => "Nom du site", 'required' => false, 'data' => $this->titre])
            ->add("description", TextType::class, ['label' => "Description", 'required' => false, 'data' => $this->footerInfo])
            ->add('couleur', ColorType::class, ['data' => $this->couleur])
            ->add("modifier", SubmitType::class, ['label' => "Modifier"]);

        return $form->getForm();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getPrincipale(): ?string
    {
        return $this->principale;
    }

    public function setPrincipale(?string $principale): self
    {
        $this->principale = $principale;

        return $this;
    }

    public function getFooterInfo(): ?string
    {
        return $this->footerInfo;
    }

    public function setFooterInfo(?string $footerInfo): self
    {
        $this->footerInfo = $footerInfo;

        return $this;
    }

    public function getImageFond(): ?string
    {
        return $this->imageFond;
    }

    public function setImageFond(?string $imageFond): self
    {
        $this->imageFond = $imageFond;

        return $this;
    }
}
