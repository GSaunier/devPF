<?php

namespace App\Entity;

use App\Utilitaire;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagesRepository")
 */
class Images extends Contenus
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom;

    public function getChemin(): string
    {
        return "/Images/Images/" . $this->getId() . "/" . $this->nom;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getType()
    {
        return "image";
    }

    public function onDelete()
    {
        Utilitaire::deleteDirectory("Images/Images/" . $this->getId());
    }

    public function genererRendu(\Twig_Environment $twig, FormFactory $form, ObjectManager $em, Request $request): ?string
    {
        $f = $this->createForm($form);
        $f->handleRequest($request);

        if ($f->isSubmitted() && $f->isValid() && $request->request->has("texte".$this->getId())) {
            $this->executeForm($f,$em);
        }

        $param = $em->getRepository(Parametres::class)->find(0);

        $this->html = $twig->render('contenu/' . $this->getType() .
            '.html.twig', [
            "t" => $this,
            "f" => $f->createView(),
            "p" => $param
        ]);
        return null;

    }

    public function createForm(FormFactory $form): FormInterface
    {
        $form = $form->createNamedBuilder("texte".$this->getId());
        $form->add("titre", TextType::class, ['data' => $this->getTitre(), 'label' => false, 'attr' => ['data-length' => 100, 'maxlength' => 100, 'class' => "text-input"]])
            ->add('img', FileType::class, ['label' => 'Image'])
            ->add("valider", SubmitType::class, ['label' => 'Valider']);
        return $form->getForm();
    }

    public function executeForm(FormInterface $form, ObjectManager $em)
    {
        $data = $form->getData();
        /** @var UploadedFile $file */
        $file = $data['img'];
        if(!is_null($file)) {
            $this->nom = ($file->getClientOriginalName());
            array_map('unlink', glob("Images/Images/" . $this->getId() . '/*'));
            $file->move("Images/Images/" . $this->getId() . '/', $this->nom);
        }
        $this->setTitre($data['titre']);
        $em->flush();
    }
}
