<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use PayPal\Api\Item;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OptionInscriRepository")
 */
class OptionInscri
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscription", inversedBy="optionInscris")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inscription;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OptionCourse", inversedBy="optionInscris")
     * @ORM\JoinColumn(nullable=false)
     */
    private $optionCourse;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInscription(): ?Inscription
    {
        return $this->inscription;
    }

    public function setInscription(?Inscription $inscription): self
    {
        $this->inscription = $inscription;

        return $this;
    }

    public function getOptionCourse(): ?OptionCourse
    {
        return $this->optionCourse;
    }

    public function setOptionCourse(?OptionCourse $optionCourse): self
    {
        $this->optionCourse = $optionCourse;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getItem(): Item {
        /** @var Item $item */
        $item = $this->optionCourse->getItem();
        $item->setQuantity($this->quantite);
        return $item;
    }

    public function getTexte() {
        return $this->optionCourse->getTitre().' ('.$this->quantite.')';
    }
}
