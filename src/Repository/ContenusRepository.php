<?php

namespace App\Repository;

use App\Entity\Contenus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Contenus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contenus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contenus[]    findAll()
 * @method Contenus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContenusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Contenus::class);
    }

    public function updateOrdre(Contenus $c,bool $add) {
        $q = $this->createQueryBuilder('c');
        $q->update("App:Contenus", 'c');
        if($add)
            $q->set('c.ordre', 'c.ordre + 1');
        else
            $q->set('c.ordre', 'c.ordre - 1');

        $q->where('c.ordre >= :ordre')
            ->setParameter('ordre', $c->getOrdre())
            ;
        if($c->getPageRef() != null) {
            $q->andWhere("c.pageRef = :idPage")
                ->setParameter('idPage', $c->getPageRef()->getId());
        } else if ($c->getContenuRef() != null) {
            $q->andWhere("c.contenuRef = :idContenu")
                ->setParameter('idContenu', $c->getContenuRef()->getId());
        } else
            return;

        $q->getQuery()->execute();
    }

    /**
     * @return Contenus[] Returns an array of Contenus objects
     */
    public function findByPageByOrdre(int $idPage, int $ordre)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.pageRef = :page')
            ->andWhere('c.ordre = :ordre')
            ->setParameter('page', $idPage)
            ->setParameter('ordre', $ordre)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Contenus[] Returns an array of Contenus objects
     */
    public function findByContenuByOrdre(int $idContenu, int $ordre)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.contenuRef = :contenu')
            ->andWhere('c.ordre = :ordre')
            ->setParameter('contenu', $idContenu)
            ->setParameter('ordre', $ordre)
            ->getQuery()
            ->getResult();
    }



//    /**
//     * @return Contenus[] Returns an array of Contenus objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contenus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
