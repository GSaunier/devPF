<?php

namespace App\Repository;

use App\Entity\Multi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Multi|null find($id, $lockMode = null, $lockVersion = null)
 * @method Multi|null findOneBy(array $criteria, array $orderBy = null)
 * @method Multi[]    findAll()
 * @method Multi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MultiRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Multi::class);
    }

    // /**
    //  * @return Multi[] Returns an array of Multi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Multi
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
