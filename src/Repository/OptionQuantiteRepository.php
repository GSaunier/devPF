<?php

namespace App\Repository;

use App\Entity\OptionQuantite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OptionQuantite|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionQuantite|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionQuantite[]    findAll()
 * @method OptionQuantite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionQuantiteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OptionQuantite::class);
    }

//    /**
//     * @return OptionQuantite[] Returns an array of OptionQuantite objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OptionQuantite
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
