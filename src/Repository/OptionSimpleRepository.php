<?php

namespace App\Repository;

use App\Entity\OptionSimple;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OptionSimple|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionSimple|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionSimple[]    findAll()
 * @method OptionSimple[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionSimpleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OptionSimple::class);
    }

//    /**
//     * @return OptionSimple[] Returns an array of OptionSimple objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OptionSimple
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
