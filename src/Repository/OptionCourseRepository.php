<?php

namespace App\Repository;

use App\Entity\OptionCourse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OptionCourse|null find($id, $lockMode = null, $lockVersion = null)
 * @method OptionCourse|null findOneBy(array $criteria, array $orderBy = null)
 * @method OptionCourse[]    findAll()
 * @method OptionCourse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OptionCourseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OptionCourse::class);
    }

//    /**
//     * @return OptionCourse[] Returns an array of OptionCourse objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OptionCourse
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
