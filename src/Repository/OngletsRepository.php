<?php

namespace App\Repository;

use App\Entity\Onglets;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Onglets|null find($id, $lockMode = null, $lockVersion = null)
 * @method Onglets|null findOneBy(array $criteria, array $orderBy = null)
 * @method Onglets[]    findAll()
 * @method Onglets[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OngletsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Onglets::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function findOnglets(){
        return $this->createQueryBuilder('o')
            ->select('o as onglet, o.nom, o.afficher')
            ->indexBy('o','o.id')
            ->where('o.ongletRef is null')
            ->andWhere('o.ordre is not null')
            ->orderBy('o.ordre', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function updateOrdre(Onglets $c,bool $add) {
        $q = $this->createQueryBuilder('o');
        $q->update("App:Onglets", 'o');
        if($add)
            $q->set('o.ordre', 'o.ordre + 1');
        else
            $q->set('o.ordre', 'o.ordre - 1');

        $q->where('o.ordre >= :ordre')
            ->setParameter('ordre', $c->getOrdre())
        ;
        if($c->getOngletRef() != null) {
            $q->andWhere("o.ongletRef = :idRef")
                ->setParameter('idRef', $c->getOngletRef()->getId());
        } else
            $q->andWhere("o.ongletRef is null");

        $q->getQuery()->execute();
    }

    /**
     * @return Onglets
     */
    public function findByOrdre(int $ordre)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.ongletRef is null')
            ->andWhere('o.ordre = :ordre')
            ->setParameter('ordre', $ordre)
            ->getQuery()
            ->getResult()[0];
    }

    /**
     * @return Onglets
     */
    public function findByOrdreByOnglet(int $ordre, int $ongletRef)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.ongletRef = :id')
            ->andWhere('o.ordre = :ordre')
            ->setParameter('id',$ongletRef)
            ->setParameter('ordre', $ordre)
            ->getQuery()
            ->getResult()[0];
    }

//    /**
//     * @return Onglets[] Returns an array of Onglets objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Onglets
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
